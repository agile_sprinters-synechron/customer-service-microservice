/*
package com.banking.customerservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;

@Configuration
public class ApplicationSecurityConfiguration extends WebSecurityConfigurerAdapter {

	

	
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/login**", "/contact-us/**", "/h2-console/**", "/actuator/**")
                    .permitAll()
                .antMatchers(HttpMethod.GET, "/api/customer/**")
                    .hasAnyAuthority("ROLE_Everyone", "ROLE_users", "ROLE_admins")
                .antMatchers(HttpMethod.POST, "/api/ticket/customer/**")
                    .hasAnyAuthority("ROLE_admins")
                .antMatchers(HttpMethod.PUT, "/api/customer/**")
                    .hasAnyAuthority("ROLE_super_admins")
                .anyRequest()
                    .fullyAuthenticated()
                .and()
                .oauth2ResourceServer()
                .jwt();

    }
    

    @Bean
    public JwtAuthenticationConverter jwtAuthenticationProvider(){
        final JwtAuthenticationConverter jwtAuthenticationProvider = new JwtAuthenticationConverter();
        JwtGrantedAuthoritiesConverter grantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
        grantedAuthoritiesConverter.setAuthorityPrefix("ROLE_");
        grantedAuthoritiesConverter.setAuthoritiesClaimName("groups");
        jwtAuthenticationProvider.setJwtGrantedAuthoritiesConverter(grantedAuthoritiesConverter);
        return jwtAuthenticationProvider;

    }
}
*/
