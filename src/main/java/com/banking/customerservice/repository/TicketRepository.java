package com.banking.customerservice.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.banking.customerservice.model.Ticket;

public interface TicketRepository extends JpaRepository<Ticket, Integer>{

	Optional<List<Ticket>> findBycustomerid(Integer customerid);

	Optional<Ticket> findByIdAndCustomerid(Integer id, Integer customerid);

}


