package com.banking.customerservice.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Ticket {

	private Integer id;
	private Integer customerid;
	private String email_address;
	private String message;
	private String phone;
	private String status;

	public Ticket() {
	}

	public Ticket(Integer id, Integer customerid, String email_address, String message, String phone,String status) {
		this.id = id;
		this.customerid = customerid;
		this.email_address = email_address;
		this.message = message;
		this.phone = phone;
		this.status = status;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCustomerid() {
		return customerid;
	}

	public void setCustomerid(Integer customerid) {
		this.customerid = customerid;
	}

	public String getEmail_address() {
		return email_address;
	}

	public void setEmail_address(String email_address) {
		this.email_address = email_address;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Ticket [id=" + id + ", customerid=" + customerid + ", email_address=" + email_address + ", message="
				+ message + ", phone=" + phone + ", status=" + status + "]";
	}

}

