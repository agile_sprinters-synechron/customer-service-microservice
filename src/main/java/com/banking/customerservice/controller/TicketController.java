package com.banking.customerservice.controller;

import org.springframework.beans.factory.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.banking.customerservice.model.Ticket;
import com.banking.customerservice.service.TicketService;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
public class TicketController {

	private static final Logger log = LoggerFactory.getLogger(TicketController.class);

	@Autowired
	private TicketService service;

	
	
	@GetMapping("/api/ticket/customer/{customerid}")
	@ResponseBody
	public ResponseEntity<Object> listBycustomerid(@PathVariable Integer customerid) {

		Optional<List<Ticket>> ticketData = service.listAllBycustomerid(customerid);

		if (ticketData.isPresent()) {
			if (!ticketData.get().isEmpty()) {
				log.info(ticketData.get().size() + " tickets present for CustomerID:" + customerid);
				return ResponseEntity
			            .status(HttpStatus.OK)                 
			            .body(ticketData.get());
			} else {
				log.error("Tickets not present for CustomerID:" + customerid);
				 return ResponseEntity
				            .status(HttpStatus.NOT_FOUND)
				            .body("Tickets not present for CustomerID:" + customerid);
			}
		} else {
			log.error("Tickets not present for CustomerID:" + customerid);
			 return ResponseEntity
			            .status(HttpStatus.NOT_FOUND)
			            .body("Tickets not present for CustomerID:" + customerid);
		}

	}

	@GetMapping("/api/customer/{customerid}/ticket/{id}")
	@ResponseBody
	public ResponseEntity<Object> list(@PathVariable Integer customerid, @PathVariable Integer id) {
		Optional<Ticket> ticketData = service.findByIdAndCustomerid(id, customerid);

		if (ticketData.isPresent()) {
			log.info("Tickets present for: " + id + " for CustomerID " + customerid);
			return ResponseEntity
		            .status(HttpStatus.OK)                 
		            .body(ticketData.get());
			
		} else {
			log.error("No ticket present with ID:" + id + " for CustomerID " + customerid);
			 return ResponseEntity
		            .status(HttpStatus.NOT_FOUND)
		            .body("No ticket present with ID:" + id + " for CustomerID " + customerid);
		}

	}

	@PostMapping("/api/ticket/customer/{customerid}")
	public ResponseEntity<Object> add(@RequestBody Ticket ticket, @PathVariable Integer customerid) {

		try {
			Ticket ticketData = service.save(new Ticket(ticket.getId(), customerid, ticket.getEmail_address(),
					ticket.getMessage(), ticket.getPhone(), ticket.getStatus()));
			log.info("Ticket: created for CustomerID:" + customerid);
			return ResponseEntity
		            .status(HttpStatus.CREATED)                 
		            .body(ticketData);
			
		} catch (Exception e) {
			log.error("Error in creating ticket for CustomerID " + customerid);
			 return ResponseEntity
			            .status(HttpStatus.INTERNAL_SERVER_ERROR)
			            .body("Error in creating ticket for CustomerID " + customerid);
		}

	}
	
}
