package com.banking.customerservice.service;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.banking.customerservice.model.Ticket;
import com.banking.customerservice.repository.TicketRepository;
 
@Service
@Transactional
public class TicketService {
 
    @Autowired
    private TicketRepository repo;
     
    public Optional<List<Ticket>> listAllBycustomerid(Integer customerid) {
        return repo.findBycustomerid(customerid);
    }
     
    public Ticket save(Ticket ticket) {
        return repo.save(ticket);
    }
     
    public Optional<Ticket> findByIdAndCustomerid(Integer id,Integer customerid) {
        return repo.findByIdAndCustomerid(id,customerid);
    }
     
  
}
